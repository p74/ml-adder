import threading
import time
sem = threading.Semaphore()
sem2 = threading.Semaphore()

numbersList = list()
addList = list()
flag = 0
flag2 = 0

def T1():
    text = open(".\\data\\numbers.txt", errors="ignore")
    global flag

    for line in text:
        sem.acquire()
        numbersList.append(int(line))
        time.sleep(0.5)
        sem.release()

        if flag < 2:
            flag += 1
        else:
            flag = 0

        print("T1: read", numbersList[-1])

    text.close()

def T2():
    global numbersList
    global flag
    global flag2

    sem2.acquire()
    while True:
        if flag == 2:
            sem.acquire()
            #sem2.acquire()
            addList.append(numbersList[-1] + numbersList[-2])
            time.sleep(0.2)
            flag2 = 1
            sem2.release()
            sem.release()
            print("T2: added", numbersList[-1], "and", numbersList[-2])

        else:
            continue

        if len(numbersList) >= 10000:
            break

def T3():
    global addList
    global flag2

    while True:
        if flag2 == 0:
            continue
        else:
            sem2.acquire()
            print("T3 printing:", addList[-1])
            flag2 = 0
            time.sleep(0.1)
            sem2.release()
            

        if len(numbersList) >= 10000:
            break

t1 = threading.Thread(target = T1)
t1.start()
t2 = threading.Thread(target = T2)
t2.start()
t3 = threading.Thread(target = T3)
t3.start()



