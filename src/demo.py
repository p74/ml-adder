import _thread
import time

numbersList = list()
addList = list()
pc = 0

def T1():
    global numbersList
    global pc

    for i in range (100):
        time.sleep(1)
        numbersList.append(i)
        pc += 1
        print(numbersList)

def T2():
    global addList

    while (pc <= 9):
        temp = pc
        carry = 0
        time.sleep(1.5)

        for i in range (pc - temp):
            carry = (pc - temp) % 2
            addList.append(numbersList[pc] + carry)
            print(addList)

_thread.start_new_thread(T1, ())
_thread.start_new_thread(T2, ())




def print_time( threadName, delay):
   count = 0
   while count < 5:
      time.sleep(delay)
      count += 1
      print ("%s: %s" % ( threadName, time.ctime(time.time()) ))

# Create two threads as follows
#try:
#   _thread.start_new_thread( print_time, ("Thread-1", 2, ) )
#   _thread.start_new_thread( print_time, ("Thread-2", 4, ) )
#except:
#   print ("Error: unable to start thread")

#while 1:
#   pass