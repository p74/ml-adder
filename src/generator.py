from random import randint

numbersFile = open(".\\data\\numbers.txt", "w")

for _ in range (10000):
    numbersFile.write(str(randint(0, 9)) + "\n")

numbersFile.close()